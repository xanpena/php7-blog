<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Posts Panel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Posts Panel</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <h2>Posts List</h2>
                    <a class="btn btn-primary" href="<?php echo BASE_URL; ?>admin/posts/new">New Post</a>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($blogPosts as $blogPost){ ?>
                                <tr>
                                    <td><?php echo $blogPost['title']; ?></td>
                                    <td>
                                        <a href="">Edit</a>
                                        <a href="">Delete</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                    Sidebar
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <footer>
                        This is a footer...
                        <a href="<?php echo BASE_URL; ?>admin">Admin Panel</a>
                    </footer>
                </div>
            </div>
        </div>
    </body>
</html>